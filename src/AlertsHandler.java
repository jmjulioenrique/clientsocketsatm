import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Socket;

public class AlertsHandler extends Thread {
	private Socket serverSocket;

	public AlertsHandler(Socket alertSkt) throws Exception {
		this.serverSocket = alertSkt;
	}
	public void run() {
		try {
			
			while (!Thread.interrupted()) {
				BufferedReader ent = new BufferedReader(new InputStreamReader(
						serverSocket.getInputStream()));
				String sentence = ent.readLine();
				System.out.println("WARNING RECEIVED: " + sentence);
				
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
