import java.awt.Container;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.DatagramSocket;
import java.net.Socket;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			BufferedReader teclado = new BufferedReader(new InputStreamReader(
					System.in));

			//tcp socket for server data
			Socket skt = new Socket("localhost", 1234);
			
			//tcp socket for alerts with server
			Socket alertSkt = null;

			PrintWriter sal = new PrintWriter(skt.getOutputStream(), true);
			BufferedReader ent = new BufferedReader(new InputStreamReader(
					skt.getInputStream()));

			System.out.println("Insert yout credit card number:");
			String card = teclado.readLine();
			sal.println(card);
			String acept = ent.readLine();
			System.out.println("Card: " + acept);
			if (acept.equals("OK")) {
				System.out.println("Insert your pin code:");
				String pin = teclado.readLine();
				sal.println(pin); // send pin code
				acept = ent.readLine();
				while (acept.equals("RETRY")) {
					System.out.println("Insert your pin code:");
					pin = teclado.readLine();
					sal.println(pin);
					acept = ent.readLine();
				}

				System.out.println("Login: " + acept);

				// main menu
				if (acept.equals("OK")) {
					
					//tcp socket for alerts with server
				    alertSkt = new Socket("localhost", 4321);

					//ini thread alerts
					AlertsHandler alert = new AlertsHandler(alertSkt);
					alert.start();

					System.out.println("Select operation: ");
					System.out
							.println("		1) option of checking your account balance");
					System.out.println("		2) withdraw money");
					System.out.println("		3) Exit");

					String option = teclado.readLine();
					while (!option.equals("3")) {
						if (option.equals("1")) {
							sal.println("CHECKACCOUNT"); // send pin code
							String balance = ent.readLine();
							System.out.println("Balance: " + balance + " €");
						}
						if (option.equals("2")) {
							System.out.println("Amount:");
							String amount = teclado.readLine();
							sal.println("WITHDRAW/" + amount + "");
							String withdrawStatus = ent.readLine();

							if (withdrawStatus.equals("SUCCESS"))
								System.out
										.println("transaction completed successfully");
							if (withdrawStatus.equals("DENY"))
								System.out.println("transaction denied");

						}
						
						System.out.println("Select operation: ");
						System.out
								.println("		1) option of checking your account balance");
						System.out.println("		2) withdraw money");
						System.out.println("		3) Exit");
						option = teclado.readLine();
					}
					sal.println("EXIT");
					alert.interrupt();
				}

			}
			skt.close();
			alertSkt.close();
		} catch (Exception e) {
			System.out.print("ERROR 404 \n");
		}
	}

}
